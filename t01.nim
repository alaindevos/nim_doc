#import modules
import os,system,strutils,sequtils,math,times,sugar,parseutils
import strfmt,times,locks,macros,json,algorithm,tables,sequtils
import terminal

echo "Starting"

#Comments
#This is a Comments

#[
A 
multine
comment
]#

#discard code
discard """
blablabla
blablabla
blablabla
"""

#echo,write 
echo ("Hallo")
echo "Hallo"
# First concatenating the strings and passing as single arg to echo
echo $100 & "abc" & $2.5
# Passing multiple args to echo, and letting echo "concatenate" them
echo 100, "abc", 2.5
stdout.writeLine "Hello World!"
stdout.write     "Hello World!"
setBackGroundColor(bgGreen)
stdout.styledWrite(fgRed, "red text ") 
resetAttributes()

#Mutable variables
var
    a:int=1
    #type inferred
    b=a
    a1: int8 = 0x7F 
    b1: uint8 = 0b1111_1111
    c1: int64 = 1
    d1: int = 0xFF 
    
#Immutable variables,computed at compile time
const
    c:int=2
    #type inferred 
    d=c
    
#Immutable variables,computed at run time
let
    e:int=3
    #type inferred
    f=a+c+e 
#integer division
echo "5 div 2=",5 div 2
#Float division
echo "5/2=",5/2
#An integer
echo "An integer:",9_200_000_000_000_000_000
#Shift
echo "shift:", 1.uint.shl(2)
#A float
var 
    g:float= -3.5
    h= -4.5

#subranges
type
    dieFaces=range[1..6]
var
    adie:dieFaces=3

#Conversion
echo "Conversion (i.e. drop fraction):",h.int
echo "Conversion:",int(h)
echo "Conversion","1234".parseInt()

#Stupid naming convention
var
    aa=2
b=aa+aA+a_a
echo b

#enums & ordinals
type
#Unique no prefix needed
    Compas=enum
        north,east,south,west
#Prefix needed when non unique
    Colors{.pure.}=enum
        Myred="F00"
        MyGreen="0F0"
        MyBlue="00F"

#var south:string="5" :Error
echo "ordinal:",ord(south)
echo "ordinal:",ord(Compas.south)
echo "ordinal:",ord(MyGreen)
echo "ordinal:",ord(Colors.MyGreen)
var MyGreen:string="5"    #No error
echo "ordinal:",ord(Colors.MyGreen)

#Sets
type persons = enum
    alain,eddy,emiel,helene
var s8:set[persons]
s8.incl alain
s8.incl eddy
s8.excl eddy
echo s8+{emiel,helene}
if eddy in s8:
    echo "eddy in x"
    
#Char and string
var
    cx:char='c'
    s1:string="astring"
    s2="bstring"
#Concatination
    s3=s1 & s2
#add
s3.add("cstring")
echo "String:",s3

#splitlines
for aline in splitLines("a\nb\nc\n"):
    echo aline

#Multiline
let adoc="""
Ik
Ben
Veel
"""
echo adoc

#Boolean
var
    y=true
    n=false
#relational
echo "1==1:",1==1
echo "1!=1:",1!=1
#logical
echo "true and false:",true and false
echo "done"

#If
proc mytrue():bool=
    return true
var x8=if mytrue(): 5
    else: 3

#array :homogenous container of a constant length
type
  ThreeStringAddress = array[3, string]
var names: ThreeStringAddress = ["Jasmine", "Ktisztina", "Kristof"]

const alen:int=3
var ar: array[alen,int]=[0,1,2]

#Arrays are passed read-only
proc foobar(z: array[0..3, int]) = 
#    z[5] = 5  # Error: Cannot assign to z
    return
foobar([1, 2, 3, 4])

#Lookup table
type
  PartsOfSpeech {.pure.} = enum
    Pronoun, Verb, Article, Adjective, Noun, Adverb
var partOfSpeechExamples: array[PartsOfSpeech, string] = [
  "he", "reads", "the", "green", "book", "slowly"
]
echo partOfSpeechExamples[Adverb]

#Matrix
type
  Matrix[W, H: static[int]] =
    array[1..W, array[1..H, int]]

let mat1: Matrix[2, 2] = [[1, 0],
                          [0, 1]]
let mat2: Matrix[2, 2] = [[0, 1],
                          [1, 0]]

proc `+`[W, H](a, b: Matrix[W, H]):
               Matrix[W, H] =
  for i in 1..high(a):
    for j in 1..high(a[0]):
      result[i][j] = a[i][j] + b[i][j]
echo mat1 + mat2

proc `$`(a: array[2, array[2, int]]): string =
    result = ""
    for v in a:
        for vx in v:
          result.add($vx & ", ")
        result.add("\n")
echo([[1, 2], [3, 4]])  # See varargs for how echo works

#seq :homogenous container of a variable length,dynamicle expandable
#Allocated on the heap , not on the stack
var 
    se:  seq[int]= newSeq[int](2)
    sea: seq[int] = @[]
    seb           = @[1,2,3]
    sec           =newSeq[int](2)
se[0]=0 
se[1]=1
se.add(2)
echo "seq:",se," len:",se.len 
var
    se2:seq[int]
se2= @[0,1,2,3,4,5,6,7,8,9]
#square se2
for i,v in se2:
    se2[i]=v*v
#Remove first element (slow)
se2.delete(0)
#Add an element
se2 = 0 & se2

#index
echo "index 2 from start:",se2[3]
echo "index 2 from end:",se2[^3]
#slice
echo se2[3..7]

#List comprehension
let x = @[1, 2, 3, 4, 5, 8, 8, 8, 10]  # create a Nim sequence
# syntax: lc[ item | ( item <- seq, (optional condition) ), type ]
echo lc[ y*2 | ( y <- x ), int ]

#Tuple: Fixed size, heterogenous date
var 
    at=("aaaa",2,3.5)
echo at

#Tuple with field names
#Its a composition of named and typed fields in a specific order:
type
  Person = tuple[name: string,size: int,length: float]
var
        aperson: Person
aperson=(name:"Alain",size:42,length:170.5)
aperson=("Alain",42,170.5)

var
        bperson: tuple[name: string,size: int,length: float]
bperson=(name:"Alain",size:42,length:170.5)
echo "name:",aperson.name
echo "name:",bperson[0]

#Table dictonaries
var a4 = {"hi": 1, "there": 2}.toTable
echo a4["hi"], " ", a4.len
assert a4.hasKey("hi")
for key, value in a4:
    echo key, " " ,value

#type (OBJECTS)
#Objects support inheritance (not multiple inheritance).

type
    myint=int
var 
    aint:myint

type
    Animal=object
        name:string
        species:string 
        age:int

proc sleep(a: var Animal)=
    a.age = a.age+1
    
var carl:Animal=Animal(name : "Carl",
                       species : "L. glama",
                       age : 12
                      )
carl.sleep()

#Example 
type Animal4 = ref object of RootObj
  age: int
  name: string

type Cat4 = ref object of Animal4
  playfulness: float

# A proc that can access and *modify* the object
proc increase_age(self: Cat4) =
  self.age.inc()

var c4 = Cat4(name: "Tom")
c4.increase_age()
echo c4.name, " ", c4.age

#Objects
type
    FreakInt = distinct int
proc `*` (x: FreakInt, y: int): FreakInt {.borrow.}
proc `+` (x, y: FreakInt): FreakInt =
    FreakInt((int(x) + int(y)) * 2)

#Safe reference type
#ref which is in fact also a pointer
#but its a friendlier one that does a bit 
#of automatic allocation and especially deallocation for us using the garbage collector. 
#They are allocated on the heap and not on the stack by use of the new() function
 
let jack: ref Animal = new(Animal)
jack[].name = "Mittens"
jack[].species = "P. leo"
jack[].age = 6
jack[].sleep()
#Derence operator [] can be left out for accesing values
jack.name = "Mittens"
jack.species = "P. leo"
jack.age = 6
#Or:
type
    AnimalRef = ref Animal
let joe = AnimalRef(name: "Spot",
                    species: "C. lupus",
                    age: 1
                   )
joe[].sleep()

#Unsafe reference type
var x3:int=3
var px=addr x3
echo px[]
var y3= cast[ptr int](px)[]
echo y
px=nil

#Reference to any object
type
    thing = ref object

#argument passed by reference
proc addone(a: var int):int=
    a=a+1
    return a
var
    r:int =1
    rr:int
rr=addone(r)
echo "addone(r):",addone(r)
echo "addone(r):",addone(r)

#Object orientation and inheritance
type Animal2 = ref object of RootObj
  name: string
  age: int
method vocalize(this: Animal2): string {.base.} = "..."
method ageHumanYrs(this: Animal2): int {.base.} = this.age

type Dog = ref object of Animal2
method vocalize(this: Dog): string = "woof"
method ageHumanYrs(this: Dog): int = this.age * 7

type Cat = ref object of Animal2
method vocalize(this: Cat): string = "meow"

var animals: seq[Animal2] = @[]
animals.add(Dog(name: "Sparky", age: 10))
animals.add(Cat(name: "Mitten", age: 10))

for a in animals:
  echo a.vocalize()
  echo a.ageHumanYrs()

#Blocks
block outer:
    var aa=1
    block inner:
        var bb=1
        break outer

#Parenthesis, semicolon blocks
block outer2:(
    var aaa=1;
    block inner2:(
        var bbb=1;
    )
)

#Branch
var 
    i=5
    j=5
if i<j:
    echo "i<j"
elif i>j:
    echo "i>j"
else:
    echo "i=j"

case i
of 4:
    echo "i=4"
of 5:
    echo "i=5"
else:
    echo "i is not 4 or 5"

case i:
of 4,5:
    echo "i is 4 or 5"
else: discard
#loops
for n in 0..9:
    echo n

for n in 0..<10:
    echo n

for n in countup(0,9,1):
    echo n

for n in countdown(9,0,1):
    echo n

var t=0
while t<10:
    echo t
    t=t+1

t=0    
while true:
    if t==10:
        break
    t.inc

for t in -10..10:
    if not(t in 0..9):
        continue
    echo t

#function: Procedure with no side effects
#Procedure:
proc mymax(x:int,y:int):int=
    if x>y:
        return x
    else:
        return y
echo mymax(3,5)

proc printme(s:string)=
    echo s
printme("Hallo")

proc changeme(i:var int)=
    i+=1
    echo i
var tc=5
changeme(tc)
echo tc

var 
    keep:int=1
proc increasekeep()=
    keep += 1
    echo keep
increasekeep()
increasekeep()

#Uniform function call syntax
proc myplus(x:int,y:int):int=
    return x+y
echo myplus(3,5)
echo 3.myplus(5)

#result : The result variable is a special variable that serves as an implicit return variable
proc myreturnfive():int=
    result=5
    echo "Hallo"
echo myreturnfive()

proc keepOdds(a: array[7,int]): seq[int] =
    result = @[]          
    for number in a:
        if number mod 2 == 1:
            result.add(number)
const f2:array[7,int] = [1, 6, 4, 43, 57, 34, 98]
echo keepOdds(f2)

#Forward declaration
proc plus(x, y: int): int    
echo 5.plus(10)              
proc plus(x, y: int): int =  
    return x + y

#No side effects functional programming
proc sum(x, y: int): int {. noSideEffect .} =
    x + y

#First class functions
let powersOfTwo = @[1, 2, 4, 8, 16, 32, 64, 128, 256]
echo(powersOfTwo.filter do (x: int) -> bool: x > 32)
echo powersOfTwo.filter(proc (x: int): bool = x > 32)

proc greaterThan32(x: int): bool = x > 32
echo powersOfTwo.filter(greaterThan32)

#Sugar '->' macro
proc map(str: string, fun: (char) -> char): string =
  for c in str:
    result &= fun(c)
#Sugar lambda value
echo "foo".map((c) => char(ord(c) + 1))
echo "foo".map(proc (c: char): char = char(ord(c) + 1))

#Math
echo sin(30.0.degToRad()).round(2)

#Files & Strutils
echo "abc".toUpperAscii().repeat(5)
echo "Please enter your name:"
#let name:string = readLine(stdin)
let name:string="Hallo"
const filename:string="myfile.txt"
let afile=open(filename,fmWrite)
afile.writeLine(name)
afile.writeLine(name)
afile.close
for line in lines(filename):
  echo line
#os
echo "Number of commandline arguments:",paramCount()
echo "First argument:",paramStr(0)

#Varargs
proc printThings(things: varargs[string]) =
  for thing in things:
    echo thing
printThings "words", "to", "print"

#FFI:
proc strcmp(a, b: cstring): cint {.importc: "strcmp", nodecl.}
echo strcmp("C?", "Easy!")

#Calling a C function
proc printf(formatstr: cstring) {.header: "<stdio.h>", varargs.}
var xxx = "foo"
printf("Hello %d %s!\n", 12, xxx)

#Testing the os
when defined(windows):
  const imglib = "a"
elif defined(macosx):
  const imglib = "b"
else:
  const imglib = "c"
echo imglib

#Strfmt
echo "Hello {} number {:04.1f}".fmt("World", 6.0)

#Time
proc mylog(text: string) =
  echo "[$# $#]: $#" % [getDateStr(), getClockStr(), text]
mylog "Hello world"

#Template language extension
template times(integer: int, code_to_repeat: untyped): untyped =
    for i in 1..integer:
        code_to_repeat
        
4.times:
    echo "Hello World"
times(4):
    echo "Hello World"

#Rewrite a and a as a
template optLog1{a and a}(a): auto = a

#Template (no function call)
template mylog2(text: string) =
    echo "[$# $#]: $#" % [getDateStr(), getClockStr(), text]
mylog2 "Hello world"
var x9 = 12
var y9 = 3.4
mylog2 "{} + {:.2f} == {}".fmt(x9, y9, x9.float + y9)
mylog2  interp"I have $x Bananas!"

#Generics
proc mymin[T](x, y: T): T =
  if x < y: x else: y
echo mymin(2, 3) # more explicitly: min[int](2, 3)
echo mymin("foo", "bar") # min[string]("foo", "bar")

#Inline iterator
iterator areverseItems(x: string): char =
  for i in countdown(x.high, x.low):
    yield x[i]
for c in "foo".areverseItems:
  echo c
  
#Generics inline iterator
iterator breverseItems[T](x: T): auto =
  for i in countdown(x.high, x.low):
    yield x[i]
for c in "foo".breverseItems:
  echo c
  
#Closure iterators
proc mypowers(m: int): auto =
    #return iterator: int {.closure.} = # Make a closure explicitly
    return iterator: int = # Compiler makes this a closure for us
        for n in 0..int.high:
            yield n^m

var
    squares = mypowers(2)
    cubes =   mypowers(3)

for i in 1..4:
    echo "Square: ", squares() # 0, 1, 4, 9
for i in 1..4:
    echo "Cube: ", cubes()     # 0, 1, 8, 27
echo "Square: ", squares()   # 16
echo "Cube: ", cubes()       # 64

#Template locking
template withLock(lock: Lock, body: untyped) =
    acquire lock
    try:
        body
    finally:
        release lock
var lock:Lock
initLock lock
withLock lock:
    echo "a"
    echo "b"
    
#Macros
macro m(): untyped =
    result = parseStmt"""
echo "foo"
echo "bar"
    """
m()
